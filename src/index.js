import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/style.css'
import Layout from './components/layout';
import Home from './views/home';
import Sobre from './views/sobre';

ReactDOM.render(
  <React.Fragment>
    <Layout page="Home">
      {/* <Home /> */}
      <Home />
    </Layout>
  </React.Fragment>,
  document.getElementById('root')
);

