import Header from './header'

const Layout = ({ children, page }) => {
  document.title = page;
  return (
    <div>
      <Header page={page} />
      <main>
        { children }
      </main>
      <footer>
        <p>Todos los derechos reservados cabrón</p>
      </footer>
    
      
    </div>
  )
}

export default Layout;