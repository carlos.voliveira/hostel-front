const Header = (props) => {
  return (
    <div>
      <header>
        <h1>projeto { props.page }</h1>
      </header>
    </div>
  )
}

export default Header