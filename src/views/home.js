import React, { useState, useEffect } from 'react';

import axios from 'axios';
const urlApi = "https://projeto-02-backend.herokuapp.com/v1/";



const Home = () => {
  //controle de estado local
  // const [ valor atual, função que muda o valor ] = React.useState();
  const [ nome, setNome] = useState("carlos.voliveira");
  const [ quartos, setQuartos ] = useState([]);

  /*
    1. chamar a api
    2. guardar o resultado da api em um estado
  */
  
  useEffect(() => {
    const getQuartos = async () => {
      const res = await axios.get(urlApi);
      setQuartos(res.data)

    };

    getQuartos();



  }, [])
  // const mudarNome = () => {
  //   // console.log('testando a função')
  //   setNome('teste');
  // }

  return (
    <div className="quartos">
      {/* { JSON.stringify(quartos) } */}
      { quartos.map(item =>(
        <li>{item.descricao}</li>
      ))}
      <ul>
        <li>Quarto Individual</li>
        <li>Quarto Coletivo</li>
        <li>Suite</li>
      </ul>
      <hr />

      <button onClick={ () => setNome('teste') }> 
        {/* <button onClick={ mudarNome }> */}
          botão para mudar o nome
      </button>
    </div> 
  );
}

export default Home;