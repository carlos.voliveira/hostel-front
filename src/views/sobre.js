import React from 'react'

const Sobre = () => {

  const [salario, setSalario] = React.useState(1000)
  const [info] = React.useState({
    falta: 10,
    hExtra: 100
  })

  const add = () => {
    console.log('irá adicionar no salário')
    setSalario(salario + info.hExtra)
  }

  const desc = () => {
    console.log('irá descontar no salário')
    setSalario(salario - info.falta)
  }

  return (
    <div>
      <strong>Salário:</strong> { salario }
      <hr />
      <button onClick={add}>Acrescentar uma comissão + 100</button>
      <br />
      <br />
      <button onClick={desc}>desconto por falta +10</button>
    </div>
  )
}

export default Sobre